package Animal;

public abstract class LandAnimal extends Animal {
    public LandAnimal(String name, int numberOfLeg){
        super(name, numberOfLeg);
    }

    public abstract void run();
}
