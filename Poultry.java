package Animal;

public abstract class Poultry extends Animal {
    public Poultry(String name, int numberOfLeg){
        super(name, numberOfLeg);
    }

    public abstract void fly();
}
