package Animal;

public class Cat extends LandAnimal {
    private String nickname;

    public Cat(String nickname){
        super("Cat", 4);
        this.nickname = nickname;
    }
    
    @Override
    public void run() {
        System.out.println("Cat: " + nickname + " run");
        
    }

    @Override
    public void eat() {
        System.out.println("Cat: " + nickname + " eat");
        
    }

    @Override
    public void walk() {
        System.out.println("Cat: " + nickname + " walk");
        
    }

    @Override
    public void speak() {
        System.out.println("Cat: " + nickname + " speak");
        
    }

    @Override
    public void sleep() {
        System.out.println("Cat: " + nickname + " sleep");
        
    }
}
