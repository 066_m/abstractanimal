package Animal;

public abstract class Reptile extends Animal {
    public Reptile(String name, int numberOfLeg){
        super(name, numberOfLeg);
    }

    public abstract void crawl();
}
