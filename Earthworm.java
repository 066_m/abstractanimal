package Animal;

public class Earthworm extends Animal{
    private String nickname;

    public Earthworm(String nickname){
        super("Earthworm", 0);
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        System.out.println("Earthworm: " + nickname + " eat");
        
    }

    @Override
    public void walk() {
        System.out.println("Earthworm: " + nickname + " burrow");
        
    }

    @Override
    public void speak() {
        System.out.println("Earthworm: " + nickname + " speak");
        
    }

    @Override
    public void sleep() {
        System.out.println("Earthworm: " + nickname + " sleep");
        
    }
}
