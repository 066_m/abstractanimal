package Animal;

public class Fish extends AquaticAnimal {
    private String nickname;

    public Fish(String nickname){
        super("Fish");
        this.nickname = nickname;
    }

    @Override
    public void swim() {
        System.out.println("Fish: " + nickname + " swim");
        
    }

    @Override
    public void eat() {
        System.out.println("Fish: " + nickname + " eat");
        
    }

    @Override
    public void walk() {
        System.out.println("Fish: " + nickname + " walk");
        
    }

    @Override
    public void speak() {
        System.out.println("Fish: " + nickname + " speak");
        
    }

    @Override
    public void sleep() {
        System.out.println("Fish: " + nickname + " sleep");
        
    }

    
}
