package Animal;

public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Mook");
        h1.eat();
        h1.sleep();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));
        Animal a1 = h1;
        System.out.println("a1 is land animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is aquatic animal ? " + (a1 instanceof AquaticAnimal));

        Dog d1 = new Dog("Zero");
        d1.walk();
        d1.speak();

        Cat c1 = new Cat("Tuanoi");
        c1.run();
        c1.eat();

        Crocodile cr1 = new Crocodile("Chalawan");
        cr1.walk();
        cr1.eat();
        cr1.speak();

        Snake s1 = new Snake("Naka");
        s1.crawl();
        s1.sleep();

        Fish f1 = new Fish("Charnk");
        f1.swim();
        f1.eat();
        f1.sleep();

        Crab cb1 = new Crab("Goly");
        cb1.walk();
        cb1.speak();

        Bat b1 = new Bat("David");
        b1.eat();
        b1.fly();
        b1.sleep();

        Bird bd1 = new Bird("Somsri");
        bd1.speak();
        bd1.walk();

        Earthworm e1 = new Earthworm("Robert");
        e1.eat();
        e1.walk();
        e1.speak();
        e1.sleep();
    }
}
