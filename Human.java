package Animal;

public class Human extends LandAnimal {
    private String nickname;

    public Human(String nickname){
        super("Human", 2);
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println("Human: " + nickname + " run");
        
    }

    @Override
    public void eat() {
        System.out.println("Human: " + nickname + " eat");
        
    }

    @Override
    public void walk() {
        System.out.println("Human: " + nickname + " walk");
        
    }

    @Override
    public void speak() {
        System.out.println("Human: " + nickname + " speak");
        
    }

    @Override
    public void sleep() {
        System.out.println("Human: " + nickname + " sleep");
        
    }
}
