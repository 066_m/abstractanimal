package Animal;

public class Bird extends Poultry {
    private String nickname;

    public Bird(String nickname){
        super("Bird", 2);
        this.nickname = nickname;
    }

    @Override
    public void fly() {
        System.out.println("Bird: " + nickname + " fly");
        
    }

    @Override
    public void eat() {
        System.out.println("Bird: " + nickname + " eat");
        
    }

    @Override
    public void walk() {
        System.out.println("Bird: " + nickname + " walk");
        
    }

    @Override
    public void speak() {
        System.out.println("Bird: " + nickname + " speak");
        
    }

    @Override
    public void sleep() {
        System.out.println("Bird: " + nickname + " sleep");
        
    }
}
