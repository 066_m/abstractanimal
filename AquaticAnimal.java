package Animal;

public abstract class AquaticAnimal extends Animal {
    public AquaticAnimal(String name){
        super(name, 0);
    }

    public abstract void swim();
}
